#!/usr/bin/python

import pandas as pd
import jaydebeapi
import time
import datetime
import os
now = datetime.datetime.now()
date_time=now.strftime("%d.%m.%Y %H:%M:%S")

conn = jaydebeapi.connect(
'oracle.jdbc.driver.OracleDriver',
'jdbc:oracle:thin:demipt2/peregrintook@de-oracle.chronosavant.ru:1521/deoracle',
['demipt2','peregrintook'],
'/home/demipt2/ojdbc8.jar'
)
conn.jconn.setAutoCommit(False)
curs = conn.cursor()

#1.Loading table of clients

curs.execute ("DELETE FROM DEMIPT2.BNDR_STG_CLIENTS")

conn.commit()

curs.execute ("DELETE FROM DEMIPT2.BNDR_STG_CLIENTS_DEL")

conn.commit()

curs.execute ("""INSERT INTO DEMIPT2.BNDR_STG_CLIENTS ( 
		CLIENT_ID, LAST_NAME, FIRST_NAME,
		PATRONYMIC, DATE_OF_BIRTH, PASSPORT_NUM, PASSPORT_VALID_TO,
		PHONE, CREATE_DT, UPDATE_DT )
SELECT CLIENT_ID, LAST_NAME, FIRST_NAME, PATRONYMIC, DATE_OF_BIRTH, PASSPORT_NUM,
        PASSPORT_VALID_TO, PHONE, CREATE_DT, UPDATE_DT
FROM BANK.CLIENTS
WHERE CREATE_DT > (
	SELECT COALESCE (LAST_UPDATE_DT, to_date ('1899-01-01', 'YYYY-MM-DD') )
	FROM DEMIPT2.BNDR_META_BANK  WHERE TABLE_DB = 'BANK' AND TABLE_NAME = 'CLIENTS')
	OR UPDATE_DT > ( 
	SELECT COALESCE (LAST_UPDATE_DT, to_date ('1899-01-01', 'YYYY-MM-DD') )
	FROM DEMIPT2.BNDR_META_BANK 
	WHERE TABLE_DB = 'BANK' AND TABLE_NAME = 'CLIENTS')""")

time.sleep(1)
conn.commit()

curs.execute ("""INSERT INTO DEMIPT2.BNDR_STG_CLIENTS_DEL ( CLIENT_ID )
				 SELECT CLIENT_ID FROM BANK.CLIENTS""")
conn.commit()

curs.execute ("""INSERT INTO DEMIPT2.BNDR_STG_CLIENTS ( CLIENT_ID, LAST_NAME, FIRST_NAME,
		PATRONYMIC, DATE_OF_BIRTH, PASSPORT_NUM, PASSPORT_VALID_TO,
		PHONE, CREATE_DT, UPDATE_DT )
SELECT CLIENT_ID, LAST_NAME, FIRST_NAME, PATRONYMIC, DATE_OF_BIRTH, PASSPORT_NUM,
        PASSPORT_VALID_TO, PHONE, CREATE_DT, UPDATE_DT
FROM BANK.CLIENTS
WHERE COALESCE ( UPDATE_DT, CREATE_DT ) > ( SELECT LAST_UPDATE_DT 
											FROM DEMIPT2.BNDR_META_BANK
               								WHERE TABLE_DB = 'BANK' AND TABLE_NAME = 'CLIENTS') 
""")

conn.commit()
time.sleep(1)

curs.execute ("""MERGE INTO DEMIPT2.BNDR_DWH_DIM_CLIENTS tgt
USING DEMIPT2.BNDR_STG_CLIENTS stg
ON ( tgt.CLIENT_ID = stg.CLIENT_ID )
WHEN MATCHED THEN UPDATE SET
    tgt.LAST_NAME = stg.LAST_NAME,
    tgt.FIRST_NAME = stg.FIRST_NAME,
    tgt.PATRONYMIC = stg.PATRONYMIC,
    tgt.DATE_OF_BIRTH = stg.DATE_OF_BIRTH,
    tgt.PASSPORT_NUM = stg.PASSPORT_NUM,
    tgt.PASSPORT_VALID_TO = stg.PASSPORT_VALID_TO,
    tgt.PHONE = stg.PHONE,
    tgt.CREATE_DT = stg.CREATE_DT,
    tgt.UPDATE_DT = stg.UPDATE_DT
WHEN NOT MATCHED THEN INSERT ( CLIENT_ID, LAST_NAME, FIRST_NAME,
		PATRONYMIC, DATE_OF_BIRTH, PASSPORT_NUM, PASSPORT_VALID_TO,
		PHONE, CREATE_DT, UPDATE_DT ) VALUES (stg.CLIENT_ID, stg.LAST_NAME, stg.FIRST_NAME,
		stg.PATRONYMIC, stg.DATE_OF_BIRTH, stg.PASSPORT_NUM, stg.PASSPORT_VALID_TO,
		stg.PHONE, stg.CREATE_DT, stg.UPDATE_DT ) """)

conn.commit()
time.sleep(1)

curs.execute ("""DELETE FROM DEMIPT2.BNDR_DWH_DIM_CLIENTS
WHERE CLIENT_ID IN (
    SELECT tgt.CLIENT_ID
    FROM DEMIPT2.BNDR_DWH_DIM_CLIENTS tgt
    LEFT JOIN DEMIPT2.BNDR_STG_CLIENTS_DEL stg
    ON tgt.CLIENT_ID = stg.CLIENT_ID 
    WHERE stg.CLIENT_ID IS NULL
)""")

conn.commit()
time.sleep(1)

curs.execute ("""UPDATE DEMIPT2.BNDR_META_BANK
SET LAST_UPDATE_DT = (SELECT MAX (COALESCE (UPDATE_DT, CREATE_DT)) FROM DEMIPT2.BNDR_STG_CLIENTS)
WHERE 1 = 1
    AND TABLE_DB = 'BANK'
    AND TABLE_NAME = 'CLIENTS'
    AND (SELECT MAX (COALESCE (UPDATE_DT, CREATE_DT )) FROM DEMIPT2.BNDR_STG_CLIENTS ) IS NOT NULL """)

conn.commit()
time.sleep(1)

#2.Loading table of accounts

curs.execute ("DELETE FROM DEMIPT2.BNDR_STG_ACCOUNTS")

conn.commit()

curs.execute ("DELETE FROM DEMIPT2.BNDR_STG_ACCOUNTS_DEL")

conn.commit()

curs.execute ("""INSERT INTO DEMIPT2.BNDR_STG_ACCOUNTS ( ACCOUNT_NUM, VALID_TO, CLIENT, CREATE_DT, UPDATE_DT )
SELECT ACCOUNT, VALID_TO, CLIENT, CREATE_DT, UPDATE_DT
FROM BANK.ACCOUNTS
WHERE CREATE_DT > (
	SELECT COALESCE (LAST_UPDATE_DT, to_date ('1899-01-01', 'YYYY-MM-DD') )
	FROM DEMIPT2.BNDR_META_BANK  WHERE TABLE_DB = 'BANK' AND TABLE_NAME = 'ACCOUNTS')
	OR UPDATE_DT > ( 
	SELECT COALESCE (LAST_UPDATE_DT, to_date ('1899-01-01', 'YYYY-MM-DD') )
	FROM DEMIPT2.BNDR_META_BANK 
	WHERE TABLE_DB = 'BANK' AND TABLE_NAME = 'ACCOUNTS')
""")

conn.commit()
time.sleep(1)

curs.execute ("""INSERT INTO DEMIPT2.BNDR_STG_ACCOUNTS_DEL ( ACCOUNT_NUM )
SELECT ACCOUNT FROM BANK.ACCOUNTS""")

conn.commit()
time.sleep(1)

curs.execute ("""MERGE INTO DEMIPT2.BNDR_DWH_DIM_ACCOUNTS tgt
USING DEMIPT2.BNDR_STG_ACCOUNTS stg
ON ( tgt.ACCOUNT_NUM = stg.ACCOUNT_NUM)
WHEN MATCHED THEN UPDATE SET
    tgt.VALID_TO = stg.VALID_TO,
    tgt.CLIENT = stg.CLIENT,
    tgt.CREATE_DT = stg.CREATE_DT,
    tgt.UPDATE_DT = stg.UPDATE_DT
WHEN NOT MATCHED THEN INSERT ( ACCOUNT_NUM, VALID_TO, CLIENT, CREATE_DT, UPDATE_DT )
VALUES ( stg.ACCOUNT_NUM, stg.VALID_TO, stg.CLIENT, stg.CREATE_DT, stg.UPDATE_DT ) """)

conn.commit()
time.sleep(1)

curs.execute ("""DELETE FROM DEMIPT2.BNDR_DWH_DIM_ACCOUNTS
WHERE ACCOUNT_NUM IN (
    SELECT tgt.ACCOUNT_NUM
    FROM DEMIPT2.BNDR_DWH_DIM_ACCOUNTS tgt
    LEFT JOIN DEMIPT2.BNDR_STG_ACCOUNTS_DEL stg
    ON tgt.ACCOUNT_NUM = stg.ACCOUNT_NUM
    WHERE stg.ACCOUNT_NUM IS NULL
)""")

conn.commit()
time.sleep(1)

curs.execute ("""UPDATE DEMIPT2.BNDR_META_BANK
SET LAST_UPDATE_DT = (SELECT MAX (COALESCE (UPDATE_DT, CREATE_DT)) FROM DEMIPT2.BNDR_STG_ACCOUNTS)
WHERE 1 = 1
    AND TABLE_DB = 'BANK'
    AND TABLE_NAME = 'ACCOUNTS'
    AND (SELECT MAX (COALESCE (UPDATE_DT, CREATE_DT )) FROM DEMIPT2.BNDR_STG_ACCOUNTS ) IS NOT NULL """)

conn.commit()
time.sleep(1)

#3.Loading table of cards

curs.execute ("DELETE FROM DEMIPT2.BNDR_STG_CARDS")

conn.commit()

curs.execute ("DELETE FROM DEMIPT2.BNDR_STG_CARDS_DEL")

conn.commit()

curs.execute ("""INSERT INTO DEMIPT2.BNDR_STG_CARDS ( CARD_NUM, ACCOUNT_NUM, CREATE_DT, UPDATE_DT )
SELECT CARD_NUM, ACCOUNT, CREATE_DT, UPDATE_DT
FROM BANK.CARDS
WHERE CREATE_DT > (
	SELECT COALESCE (LAST_UPDATE_DT, to_date ('1899-01-01', 'YYYY-MM-DD') )
	FROM DEMIPT2.BNDR_META_BANK  WHERE TABLE_DB = 'BANK' AND TABLE_NAME = 'CARDS')
	OR UPDATE_DT > ( 
	SELECT COALESCE (LAST_UPDATE_DT, to_date ('1899-01-01', 'YYYY-MM-DD') )
	FROM DEMIPT2.BNDR_META_BANK 
	WHERE TABLE_DB = 'BANK' AND TABLE_NAME = 'CARDS')""")

conn.commit()
time.sleep(1)

curs.execute ("""INSERT INTO DEMIPT2.BNDR_STG_CARDS_DEL ( CARD_NUM )
SELECT CARD_NUM FROM BANK.CARDS""")

conn.commit()
time.sleep(1)

curs.execute ("""MERGE INTO DEMIPT2.BNDR_DWH_DIM_CARDS tgt
USING DEMIPT2.BNDR_STG_CARDS stg
ON ( tgt.CARD_NUM = stg.CARD_NUM )
WHEN MATCHED THEN UPDATE SET
    tgt.ACCOUNT_NUM = stg.ACCOUNT_NUM,
    tgt.CREATE_DT = stg.CREATE_DT,
    tgt.UPDATE_DT = stg.UPDATE_DT 
WHEN NOT MATCHED THEN INSERT ( CARD_NUM, ACCOUNT_NUM, CREATE_DT, UPDATE_DT )
VALUES ( stg.CARD_NUM, stg.ACCOUNT_NUM, stg.CREATE_DT, stg.UPDATE_DT ) """)

conn.commit()
time.sleep(1)

curs.execute ("""DELETE FROM DEMIPT2.BNDR_DWH_DIM_CARDS
WHERE CARD_NUM IN (
    SELECT tgt.CARD_NUM
    FROM DEMIPT2.BNDR_DWH_DIM_CARDS tgt
    LEFT JOIN DEMIPT2.BNDR_STG_CARDS_DEL stg
    ON tgt.CARD_NUM = stg.CARD_NUM
    WHERE stg.CARD_NUM IS NULL
)""")

conn.commit()
time.sleep(1)

curs.execute ("""UPDATE DEMIPT2.BNDR_META_BANK
SET LAST_UPDATE_DT = (SELECT MAX (COALESCE (UPDATE_DT, CREATE_DT)) FROM DEMIPT2.BNDR_STG_CARDS )
WHERE 1 = 1
    AND TABLE_DB = 'BANK'
    AND TABLE_NAME = 'CARDS'
    AND (SELECT MAX (COALESCE (UPDATE_DT, CREATE_DT )) FROM DEMIPT2.BNDR_STG_CARDS ) IS NOT NULL """)

conn.commit()
time.sleep(1)

#4.Loading data 'Terminals' from .xlsx file

curs.execute ("DELETE FROM DEMIPT2.BNDR_STG_TERMINALS")

conn.commit()

curs.execute ("DELETE FROM DEMIPT2.BNDR_STG_TERMINALS_DEL")

conn.commit()

df = pd.read_excel ( 'terminals_01032021.xlsx', sheet_name = 'terminals', header = 0, index_col = None )
curs.executemany ( "insert into demipt2.BNDR_STG_TERMINALS ( \
	TERMINAL_ID, TERMINAL_TYPE, TERMINAL_CITY, TERMINAL_ADDRESS, UPDATE_DT \
	) values (?,?,?,?, to_date (current_date, 'DD-MM-YYYY'))", df.values.tolist () )

conn.commit()
time.sleep(1)

curs.execute ("""INSERT INTO DEMIPT2.BNDR_STG_TERMINALS_DEL ( TERMINAL_ID )
				 SELECT TERMINAL_ID FROM DEMIPT2.BNDR_STG_TERMINALS""")

conn.commit()
time.sleep(1)

curs.execute ("""MERGE INTO DEMIPT2.BNDR_DWH_DIM_TERMINALS tgt
USING DEMIPT2.BNDR_STG_TERMINALS stg
ON ( tgt.TERMINAL_ID = stg.TERMINAL_ID )
WHEN MATCHED THEN UPDATE SET
    tgt.TERMINAL_TYPE = stg.TERMINAL_TYPE,
    tgt.TERMINAL_CITY = stg.TERMINAL_CITY,
    tgt.TERMINAL_ADDRESS = stg.TERMINAL_ADDRESS,
    
    tgt.UPDATE_DT = stg.UPDATE_DT
WHEN NOT MATCHED THEN INSERT ( TERMINAL_ID, TERMINAL_TYPE, TERMINAL_CITY, TERMINAL_ADDRESS, 
                                UPDATE_DT )
VALUES ( stg.TERMINAL_ID, stg.TERMINAL_TYPE, stg.TERMINAL_CITY, stg.TERMINAL_ADDRESS,
         stg.UPDATE_DT )""") 

conn.commit()
time.sleep(1)

curs.execute ("""DELETE FROM DEMIPT2.BNDR_DWH_DIM_TERMINALS
WHERE TERMINAL_ID IN (
    SELECT tgt.TERMINAL_ID
    FROM DEMIPT2.BNDR_DWH_DIM_TERMINALS tgt
    LEFT JOIN DEMIPT2.BNDR_STG_TERMINALS_DEL stg
    ON tgt.TERMINAL_ID = stg.TERMINAL_ID
    WHERE stg.TERMINAL_ID IS NULL
)""")

conn.commit()
time.sleep(1)

curs.execute ("""UPDATE DEMIPT2.BNDR_META_BANK
SET LAST_UPDATE_DT = (SELECT MAX ( UPDATE_DT, CREATE_DT ) FROM DEMIPT2.BNDR_STG_TERMINALS )
WHERE 1 = 1
    AND TABLE_DB = 'DEMIPT2'
    AND TABLE_NAME = 'BNDR_DWH_DIM_TERMINALS'
    AND (SELECT MAX ( UPDATE_DT ) FROM DEMIPT2.BNDR_STG_TERMINALS ) IS NOT NULL """)

conn.commit()
time.sleep(1)

os.rename("/home/demipt2/bndr/terminals_01032021.xlsx", "/home/demipt2/bndr/archive/terminals_01032021.xlsx.backup")

#5.Loading data 'Transactions' from .txt file (.txt --> STG ---> FACT)

trns = pd.read_csv ( 'transactions_01032021.txt', decimal=',', sep = ';', header=0, dtype={"transaction_id": str, "transaction_date": str, "amount":float,"card_num":str,"oper_type":str,"oper_result":str,"terminal":str})
trns2=trns[['transaction_id', 'transaction_date', 'amount', 'card_num', 'oper_type', 'oper_result', 'terminal']]

curs.executemany ( "insert into DEMIPT2.BNDR_STG_TRANSACTION ( \
	TRANS_ID, TRANS_DATE, AMT, CARD_NUM, OPER_TYPE, OPER_RESULT, TERMINAL \
	) \
	values (?,to_date(?,'YYYY-MM-DD HH24:MI:SS'),?,?,?,?,?)",trns2.values.tolist () )

curs.execute(""" INSERT INTO DEMIPT2.BNDR_DWH_FACT_TRANSACTIONS ( TRANS_ID, TRANS_DATE, AMT, CARD_NUM, OPER_TYPE, OPER_RESULT, TERMINAL)
					SELECT TRANS_ID, TRANS_DATE, AMT, CARD_NUM, OPER_TYPE, OPER_RESULT, TERMINAL 
					FROM DEMIPT2.BNDR_STG_TRANSACTION """)


conn.commit()
time.sleep(1)

os.rename ("/home/demipt2/bndr/transactions_01032021.txt", "/home/demipt2/bndr/archive/transactions_01032021.txt.backup")

#6.Loading data 'Passport blacklist' from .xlsx file (..xlsx --> STG ---> FACT)

blpst=pd.read_excel('/home/demipt2/bndr/passport_blacklist_01032021.xlsx',sheet_name='blacklist', header=0, dtype={"passport": str, "date": str})
blpst2=blpst[['passport','date']]
blpst2['passport'] = blpst2['passport'].replace(r'\s+','',regex=True)

curs.executemany( "insert into DEMIPT2.BNDR_STG_PSSPRT_BLCKLST ( \
	passport_num, entry_dt) values (?,to_date(?,'YYYY-MM-DD HH24:MI:SS'))",blpst2.values.tolist())

curs.execute(""" INSERT INTO DEMIPT2.BNDR_DWH_FACT_PSSPRT_BLCKLST (passport_num, entry_dt)
					SELECT passport_num, entry_dt FROM DEMIPT2.BNDR_STG_PSSPRT_BLCKLST""")

conn.commit()
time.sleep(1)

conn.commit();
os.rename("/home/demipt2/bndr/passport_blacklist_01032021.xlsx", "/home/demipt2/bndr/archive/passport_blacklist_01032021.xlsx.backup")

#7.Report first day

curs.execute ("""
insert into demipt2.bndr_rep_fraud (event_dt, passport, fio, phone, event_type, report_dt)
select  distinct
    trans_date, 
    passport_num, 
    fio, 
    phone, 
    fraud, 
    current_date
from (
select 
        trans_id,
        case 
            when lead(terminal_city) over(partition by t.card_num order by trans_date)<>terminal_city  and (lead(trans_date) over(partition by t.card_num order by trans_date)-trans_date)<1 then lead(trans_date) over(partition by t.card_num order by trans_date)
            else trans_date
        end trans_date,    
        t.card_num, 
        c.account_num, 
        valid_to, 
        client, 
        oper_type, 
        amt, 
        oper_result, 
        terminal, 
        terminal_city, 
        last_name||' '||first_name||' '||patronymic as fio, 
        date_of_birth, 
        cl.passport_num, 
        phone,
        case
            when (coalesce(passport_valid_to, to_date('9999-12-31', 'YYYY-MM-DD' ))<trans_date ) then '1_type'
            when (p.passport_num is not null and trunc(trans_date)=trunc(entry_dt) ) then '1_type'
            when (valid_to<trans_date ) then '2_type'
            when lead(terminal_city) over(partition by t.card_num order by trans_date)<>terminal_city  and (lead(trans_date) over(partition by t.card_num order by trans_date)-trans_date)<1 then '3_type'
            else 'OKEY'
        end fraud
    from demipt2.bndr_dwh_fact_transactions  t
    left join demipt2.bndr_dwh_dim_cards c
    on t.card_num=c.card_num 
    left join demipt2.bndr_dwh_dim_accounts a
    on c.account_num=a.account_num  
    left join demipt2.bndr_dwh_dim_clients cl
    on client=client_id 
    left join demipt2.bndr_dwh_fact_pssprt_blcklst p
    on cl.passport_num=p.passport_num
    left join demipt2.bndr_dwh_dim_terminals e
    on terminal=terminal_id 
    where trans_date>=to_date ( '28.02.2021 23:00:00','DD.MM.YYYY HH24:MI:SS' ) and trans_date<to_date ( '02.03.2021 00:00:00','DD.MM.YYYY HH24:MI:SS' )

    )
where fraud<>'OKEY' and trans_date>=to_date ( '01.03.2021', 'dd.mm.yyyy' ) and trans_date<to_date ( '02.03.2021', 'dd.mm.yyyy' )
""")

conn.commit()
curs.close()
conn.close()